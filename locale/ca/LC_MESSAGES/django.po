# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-05-14 17:54+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: catalog/fields.py:23
msgid "Filetype not supported."
msgstr "El tipus de fitxer no és compatible."

#: catalog/templates/catalog/base.html:71
msgid "free games BOOM"
msgstr "jocs gratuïts BOOM"

#: catalog/templates/catalog/base.html:78
#: catalog/templates/catalog/base.html:133
msgid "Find more 100000 games"
msgstr "Trobeu més 100.000 jocs"

#: catalog/templates/catalog/base.html:79
#: catalog/templates/catalog/base.html:134
#: catalog/templates/catalog/search.html:7
msgid "Search"
msgstr "Cerca"

#: catalog/templates/catalog/base.html:97
#: catalog/templates/catalog/inclusions/sort_block.html:6
msgid "New"
msgstr "Nou"

#: catalog/templates/catalog/base.html:103
msgid "Popular"
msgstr "Popular"

#: catalog/templates/catalog/base.html:114
msgid "Favourite <br>games"
msgstr "Jocs preferits a <br>"

#: catalog/templates/catalog/base.html:125
msgid "Last <br>played"
msgstr "Darrer <br>jugat"

#: catalog/templates/catalog/category.html:95
msgid "Information"
msgstr "Informació"

#: catalog/templates/catalog/game.html:90
msgid "Category"
msgstr "Categoria"

#: catalog/templates/catalog/game.html:105
msgid "Game"
msgstr "Joc"

#: catalog/templates/catalog/game.html:131
#: catalog/templates/catalog/game.html:133
msgid "PLAY"
msgstr "JUGAR"

#: catalog/templates/catalog/game.html:142
msgid "This game is not available on mobile."
msgstr "Aquest joc no està disponible al mòbil."

#: catalog/templates/catalog/game.html:174
msgid "Controls"
msgstr "Controls"

#: catalog/templates/catalog/game.html:181
msgid "Video"
msgstr "Vídeo"

#: catalog/templates/catalog/inclusions/_include_game_list.html:19
msgid "Play!"
msgstr "Jugar!"

#: catalog/templates/catalog/inclusions/category_menu.html:9
msgid "TOP CATEGORIES"
msgstr "TOP CATEGORIES"

#: catalog/templates/catalog/inclusions/game_block.html:5
msgid "votes"
msgstr "vots"

#: catalog/templates/catalog/inclusions/game_block.html:11
msgid "Doesn’t  <br>work?"
msgstr "No <br>funciona?"

#: catalog/templates/catalog/inclusions/game_block.html:17 catalog/views.py:226
msgid "Added"
msgstr "Afegit"

#: catalog/templates/catalog/inclusions/game_block.html:17
msgid "Add to <br>favorites"
msgstr "Afegir a <br>preferits"

#: catalog/templates/catalog/inclusions/game_block.html:35
msgid "Share"
msgstr "Compartir"

#: catalog/templates/catalog/inclusions/game_block.html:41
msgid "Fullscreen"
msgstr "Pantalla completa"

#: catalog/templates/catalog/inclusions/popup-games.html:7
msgid "Clear list"
msgstr "Esborra la llista"

#: catalog/templates/catalog/inclusions/sort_block.html:3
msgid "Sort by"
msgstr "Ordenar per"

#: catalog/templates/catalog/inclusions/sort_block.html:3
#: catalog/templates/catalog/inclusions/sort_block.html:5
msgid "Most Popular"
msgstr "El més popular"

#: catalog/templates/catalog/inclusions/sort_block.html:7
msgid "A-Z"
msgstr "A-Z"

#: catalog/templates/catalog/tags.html:6
msgid "Tags"
msgstr "Etiquetes"

#: catalog/templates/catalog/tags.html:11
msgid "Popular tags"
msgstr "Etiquetes populars"

#: catalog/templatetags/catalog_tags.py:45
msgid "MOST POPULAR GAMES"
msgstr "JOCS MÉS POPULARS"

#: catalog/templatetags/catalog_tags.py:64
msgid "POPULAR TAGS"
msgstr "PERSONATGES POPULARS"

#: catalog/templatetags/catalog_tags.py:87
msgid "MOST POPULAR"
msgstr "EL MÉS POPULAR"

#: catalog/templatetags/catalog_tags.py:156
msgid "FGM RECOMMENDED!"
msgstr "FGM RECOMANADA!"

#: catalog/templatetags/catalog_tags.py:160
#, python-format
msgid "MORE %(category)s"
msgstr "MÉS %(category)s"

#: catalog/templatetags/catalog_tags.py:187
msgid "LAST PLAYED"
msgstr "ÚLTIM JUGAT"

#: catalog/templatetags/catalog_tags.py:282 catalog/views.py:71
msgid "New games"
msgstr "Nous jocs"

#: catalog/views.py:70
msgid "New games - Free Games Boom.com"
msgstr "Nous jocs - Free Games Boom.com"

#: catalog/views.py:74
msgid "Popular games - Free Games Boom.com"
msgstr "Jocs populars - Free Games Boom.com"

#: catalog/views.py:75
msgid "Popular games"
msgstr "Jocs populars"

#: catalog/views.py:99
#, fuzzy, python-format
#| msgid "Popular games"
msgid "Similar %s games"
msgstr "Jocs populars"

#: catalog/views.py:191
msgid "Favourite games"
msgstr "Jocs preferits"

#: catalog/views.py:194
msgid "Last Played"
msgstr "Darrer jugat"

#: flatpages/apps.py:7
msgid "Flat Pages"
msgstr "Pàgines planes"

#: flatpages/models.py:12
msgid "Title"
msgstr "Títol"

#: flatpages/models.py:14
msgid "Text"
msgstr "Text"

#: flatpages/models.py:15
msgid "Active"
msgstr "Actiu"

#: flatpages/models.py:16
msgid "Modified"
msgstr "Modificat"

#: flatpages/models.py:19
msgid "Static pages"
msgstr "Pàgines estàtiques"

#: flatpages/models.py:20
msgid "Static page"
msgstr "Pàgina estàtica"

#: freegamesboom/settings.py:155
msgid "Afrikaans"
msgstr "Afrikaans"

#: freegamesboom/settings.py:156
msgid "Arabic"
msgstr "Àrab"

#: freegamesboom/settings.py:157
msgid "Azerbaijani"
msgstr "Azerbaidjan"

#: freegamesboom/settings.py:158
msgid "Bulgarian"
msgstr "Búlgar"

#: freegamesboom/settings.py:159
msgid "Belarusian"
msgstr "Bielorús"

#: freegamesboom/settings.py:160
msgid "Bengali"
msgstr "Bengalí"

#: freegamesboom/settings.py:161
msgid "Bosnian"
msgstr "Bosnià"

#: freegamesboom/settings.py:162
msgid "Catalan"
msgstr "Català"

#: freegamesboom/settings.py:163
msgid "Czech"
msgstr "Txec"

#: freegamesboom/settings.py:164
msgid "Welsh"
msgstr "Gal·lès"

#: freegamesboom/settings.py:165
msgid "Danish"
msgstr "Danès"

#: freegamesboom/settings.py:166
msgid "German"
msgstr "Alemany"

#: freegamesboom/settings.py:167
msgid "Greek"
msgstr "Grec"

#: freegamesboom/settings.py:168
msgid "English"
msgstr "Anglès"

#: freegamesboom/settings.py:169
msgid "Esperanto"
msgstr "Esperanto"

#: freegamesboom/settings.py:170
msgid "Spanish"
msgstr "Castellà"

#: freegamesboom/settings.py:171
msgid "Estonian"
msgstr "Estonià"

#: freegamesboom/settings.py:172
msgid "Finnish"
msgstr "Finès"

#: freegamesboom/settings.py:173
msgid "French"
msgstr "Francès"

#: freegamesboom/settings.py:174
msgid "Irish"
msgstr "Irlandès"

#: freegamesboom/settings.py:175
msgid "Galician"
msgstr "Gallec"

#: freegamesboom/settings.py:176
msgid "Hebrew"
msgstr "Hebreu"

#: freegamesboom/settings.py:177
msgid "Hindi"
msgstr "Hindi"

#: freegamesboom/settings.py:178
msgid "Croatian"
msgstr "Croata"

#: freegamesboom/settings.py:179
msgid "Hungarian"
msgstr "Hongarès"

#: freegamesboom/settings.py:180
msgid "Armenian"
msgstr "Armeni"

#: freegamesboom/settings.py:182
msgid "Italian"
msgstr "Italià"

#: freegamesboom/settings.py:183
msgid "Japanese"
msgstr "Japonès"

#: freegamesboom/settings.py:184
msgid "Georgian"
msgstr "Georgià"

#: freegamesboom/settings.py:186
msgid "Kazakh"
msgstr "Kazakh"

#: freegamesboom/settings.py:187
msgid "Khmer"
msgstr "Khmer"

#: freegamesboom/settings.py:188
msgid "Kannada"
msgstr "Kannada"

#: freegamesboom/settings.py:189
msgid "Korean"
msgstr "Coreà"

#: freegamesboom/settings.py:190
msgid "Luxembourgish"
msgstr "Luxemburguès"

#: freegamesboom/settings.py:191
msgid "Lithuanian"
msgstr "Lituà"

#: freegamesboom/settings.py:192
msgid "Latvian"
msgstr "Letó"

#: freegamesboom/settings.py:193
msgid "Macedonian"
msgstr "Macedoni"

#: freegamesboom/settings.py:194
msgid "Malayalam"
msgstr "Malaià"

#: freegamesboom/settings.py:195
msgid "Mongolian"
msgstr "Mongol"

#: freegamesboom/settings.py:196
msgid "Marathi"
msgstr "Marathi"

#: freegamesboom/settings.py:197
msgid "Burmese"
msgstr "Birmà"

#: freegamesboom/settings.py:198
msgid "Nepali"
msgstr "Nepalí"

#: freegamesboom/settings.py:199
msgid "Dutch"
msgstr "Holandès"

#: freegamesboom/settings.py:200
msgid "Ossetic"
msgstr "Ossetic"

#: freegamesboom/settings.py:201
msgid "Punjabi"
msgstr "Punjabi"

#: freegamesboom/settings.py:202
msgid "Polish"
msgstr "Polonès"

#: freegamesboom/settings.py:203
msgid "Portuguese"
msgstr "Portuguès"

#: freegamesboom/settings.py:204
msgid "Romanian"
msgstr "Romanès"

#: freegamesboom/settings.py:205
msgid "Russian"
msgstr "Rus"

#: freegamesboom/settings.py:206
msgid "Slovak"
msgstr "Eslovac"

#: freegamesboom/settings.py:207
msgid "Slovenian"
msgstr "Eslovè"

#: freegamesboom/settings.py:208
msgid "Albanian"
msgstr "Albanès"

#: freegamesboom/settings.py:209
msgid "Serbian"
msgstr "Serbi"

#: freegamesboom/settings.py:210
msgid "Swedish"
msgstr "Suec"

#: freegamesboom/settings.py:211
msgid "Swahili"
msgstr "Swahili"

#: freegamesboom/settings.py:212
msgid "Tamil"
msgstr "Tamil"

#: freegamesboom/settings.py:213
msgid "Telugu"
msgstr "Telugu"

#: freegamesboom/settings.py:214
msgid "Thai"
msgstr "Tailandès"

#: freegamesboom/settings.py:215
msgid "Turkish"
msgstr "Turc"

#: freegamesboom/settings.py:216
msgid "Tatar"
msgstr "Tàtar"

#: freegamesboom/settings.py:218
msgid "Ukrainian"
msgstr "Ucraïnès"

#: freegamesboom/settings.py:219
msgid "Urdu"
msgstr "Urdú"

#: freegamesboom/settings.py:220
msgid "Vietnamese"
msgstr "Vietnamita"

#: freegamesboom/settings.py:221
msgid "Simplified Chinese"
msgstr "Xinès simplificat"

#: templates/404.html:18
msgid "404 Error"
msgstr "Error 404"

#: templates/404.html:19
msgid "Oops. This page does not exist."
msgstr "Vaja! Aquesta pàgina no existeix."

#: templates/el_pagination/show_more.html:5
msgid "more"
msgstr "més"

#: templates/el_pagination/show_pages.html:6
msgid "All"
msgstr "Tots"

#~ msgid "Indonesian"
#~ msgstr "Indonèsia"

#~ msgid "Kabyle"
#~ msgstr "Kabyle"

#~ msgid "Udmurt"
#~ msgstr "Udmurt"
