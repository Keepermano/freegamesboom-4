from django.http import JsonResponse

from .models import PushUser


def add_token(request):
    token = request.POST.get('token')
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[-1].strip()
    else:
        ip = request.META.get('REMOTE_ADDR')

    headers = str(request.headers)
    if token:
        PushUser.objects.update_or_create(token=token, defaults={
            'ip': ip,
            'headers': headers
        })
    return JsonResponse({})
