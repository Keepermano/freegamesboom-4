# -*- coding: utf-8 -*-
import os
from urllib import request
from urllib.error import HTTPError, URLError
from tqdm import tqdm

from django.core.files import File
from django.db.utils import IntegrityError
from django.core.management.base import BaseCommand

from catalog.models import GameParse, Game, Category, Tag, GamesTranslation

site = 'http://www.agame.com'


class Command(BaseCommand):

    def handle(self, *args, **options):
        for o in tqdm(GameParse.objects.all().exclude(title='')):
            category, created = Category.objects.get_or_create(name=o.category, defaults={
                'agame': True,
                'public': False,
            })
            tags = []
            for t in o.tags.split(','):
                tags.append(Tag.objects.get_or_create(name=t, defaults={
                    'agame': True,
                    'public': False,
                })[0])
            try:
                game, created = Game.objects.get_or_create(slug=o.slug, defaults={
                    'name': o.title,
                    'category': category,
                    'agame': True,
                    'public': False,
                })
                GamesTranslation.objects.create(
                    text_block_code='description',
                    language_code='en',
                    text=o.description,
                    game_id=game.id
                )
            except IntegrityError:
                pass
            else:
                # только если игра создана (не найдено дубля по SLUG)
                if created:
                    game.tags.add(*tags)
                    if o.image:
                        image_url = o.image
                        if image_url.startswith('//'):
                            image_url = 'https://' + image_url[2:]
                        try:
                            result = request.urlretrieve(image_url)
                        except URLError:
                            image_url = image_url.replace('https', 'http')
                            result = request.urlretrieve(image_url)

                        game.image.save(
                            os.path.basename(image_url),
                            File(open(result[0], 'rb'))
                        )
                    if o.code_clear:
                        if o.code_clear.find('.swf') > 0:
                            flash_link = o.code_clear
                            if flash_link.startswith('//'):
                                flash_link = 'https://' + flash_link[2:]
                            game.flash_link = flash_link
                        else:
                            game.link = o.code_clear
                    game.save()
