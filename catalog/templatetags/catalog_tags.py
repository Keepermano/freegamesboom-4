import re
import datetime

from functools import reduce

from django import template
from django.utils.translation import get_language
from django.conf import settings as django_settings
from django.utils.translation import ugettext_lazy as _
from django.template.defaultfilters import linebreaks, safe

from catalog.models import Category, Game, Tag
from catalog.utils import mobile_order
from common.models import Settings

register = template.Library()

SCHEME = 'http' if django_settings.DEBUG else 'https'


@register.inclusion_tag('catalog/inclusions/category_menu.html', takes_context=True)
def category_menu(context, block_vertical, block_horizontal):
    objects = Category.published.all()
    return {
        'objects': objects,
        'request': context['request'],
        'scheme': SCHEME,
        'block_vertical': block_vertical,
        'block_horizontal': block_horizontal,
    }


@register.inclusion_tag('catalog/inclusions/_block_slider.html', takes_context=True)
def index_top_games_slider(context):
    return {
        'objects': mobile_order(Game.published.filter(top=True), context['request']),
        'request': context['request'],
        'scheme': SCHEME
    }


@register.inclusion_tag('catalog/inclusions/_block_simple.html', takes_context=True)
def index_popular_games(context, count=16):
    return {
        'title': _('MOST POPULAR GAMES'),
        'objects': Game.published.all().order_by('-play_counter')[:count],
        'request': context['request'],
        'scheme': SCHEME
    }


@register.inclusion_tag('catalog/inclusions/index_categories.html', takes_context=True)
def index_categories(context, count=8):
    return {
        'categories': Category.published.filter(view_on_main=True)[:count],
        'request': context['request'],
        'scheme': SCHEME
    }


@register.inclusion_tag('catalog/inclusions/_block_buttons.html', takes_context=True)
def index_tags(context):
    return {
        'title': _('POPULAR TAGS'),
        'objects': Tag.published.filter(top=True),
        'request': context['request'],
        'scheme': SCHEME
    }


@register.inclusion_tag('catalog/inclusions/_block_slider.html', takes_context=True)
def top_games_slider(context, obj):
    return {
        'objects': mobile_order(obj.games_published.filter(top=True), context['request']),
        'request': context['request'],
        'scheme': SCHEME
    }


@register.inclusion_tag('catalog/inclusions/_block_title.html', takes_context=True)
def popular_object_games(context, obj, count=16):
    return {
        'objects': mobile_order(obj.games_published.all(), context['request'])[:count],
        'request': context['request'],
        'scheme': SCHEME,
        'category': obj,
        'title': _('MOST POPULAR') + ' %s' % obj,
    }


@register.inclusion_tag('catalog/inclusions/popular_obj_game_tags.html', takes_context=True)
def popular_obj_game_tags(context, obj):
    site_settings = Settings.objects.get(pk=1)
    is_category = isinstance(obj, Category)
    if is_category:
        objects = obj.tags_new.filter(public=True)[:site_settings.count_tags]
    else:
        objects = obj.tags_new.filter(public=True).exclude(pk=obj.pk)[:site_settings.count_tags]
    return {
        'objects': objects,
        'request': context['request'],
        'scheme': SCHEME,
        'category': obj,
    }


@register.filter
def category_filter(games_qs, category):
    objects = games_qs.filter(category=category)
    return objects


@register.filter
def mobile_order_filter(games_qs, request):
    return mobile_order(games_qs, request)


@register.inclusion_tag('catalog/inclusions/block_tags_other.html', takes_context=True)
def block_tags_other(context, game=None):
    lang = get_language()
    exclude = {
        'name_{lang}__isnull'.format(lang=lang.replace('-', '_')): True
    }
    if game:
        tags = game.tags.all().exclude(**exclude)
    else:
        tags = Tag.published.all()
    return {
        'objects': tags,
        'request': context['request'],
        'scheme': SCHEME
    }


# @register.inclusion_tag('catalog/inclusions/_block_title.html', takes_context=True)
# def block_popular_games_cycle(context, game, count=32):
#     objects = mobile_order(game.category.games_published.all(), context['request'])
#     list_objects = list(objects)
#     try:
#         idx = list_objects.index(game) + 1
#         objects = list_objects[idx:][:count] if list_objects else None
#     except ValueError:
#         objects = None
#     return {
#         'title': _('MORE %(category)s') % {'category': game.category},
#         'objects': objects,
#         'request': context['request'],
#         'scheme': SCHEME
#     }


@register.inclusion_tag('catalog/inclusions/_block_title.html', takes_context=True)
def block_popular_games(context, count, game=None):
    params = {}
    exclude = {}
    title = _('FGM RECOMMENDED!')
    if game:
        exclude['pk'] = game.pk
        params['category'] = game.category
        title = _('MORE %(category)s') % {'category': game.category}
    objects = mobile_order(Game.published.filter(**params).exclude(**exclude), context['request'])[:count]
    return {
        'game': game,
        'title': title,
        'objects': objects,
        'request': context['request'],
        'scheme': SCHEME
    }


@register.inclusion_tag('catalog/inclusions/_block_title.html', takes_context=True)
def block_last_played_games(context, count=4, game=None):
    exclude_params = {}
    objects = None
    if game:
        exclude_params['pk'] = game.id
    last_played_list_ids = context['request'].session.get('last_played')
    if last_played_list_ids:
        # qs = Game.published.filter(id__in=last_played_list_ids).exclude(**exclude_params).extra(
        #     select={'manual': "FIELD(id, %s)" % ','.join(str(x) for x in last_played_list_ids)},
        #     order_by=['manual']
        # ).order_by('manual').distinct()
        qs = Game.published.filter(id__in=last_played_list_ids).exclude(**exclude_params).order_by('manual').distinct()
        objects = mobile_order(qs, context['request'])[:count]
    return {
        'game': game,
        'title': _('LAST PLAYED'),
        'objects': objects,
        'request': context['request'],
        'scheme': SCHEME
    }


@register.simple_tag
def building_meta(type, obj, site_settings):
    if type == 'title':
        meta_field_game = site_settings.tm_title_game
        meta_field_category = site_settings.tm_title_category
        meta_field_tag = site_settings.tm_title_tag
        default_meta = obj.meta_title
    else:
        meta_field_game = site_settings.tm_desc_game
        meta_field_category = site_settings.tm_desc_category
        meta_field_tag = site_settings.tm_desc_tag
        default_meta = obj.meta_desc

    if isinstance(obj, Game):
        if default_meta:
            data = default_meta
        elif meta_field_game:
            r = {'[name]': obj.name, '[category]': obj.category.name if obj.category else '', '[name2_seo]': obj.name2_seo,
                 '[name_seo_en]': obj.name_en}
            data = reduce(
                lambda a, kv: a.replace(*kv),
                r.items(),
                meta_field_game
            )
        else:
            data = ''
    elif isinstance(obj, Category):
        if default_meta:
            data = default_meta
        elif meta_field_category:
            r = {'[name]': obj.name, '[total_games]': str(obj.games_published.count()), '[name2_seo]': obj.name2_seo,
                 '[name_seo_en]': obj.name_en}
            data = reduce(
                lambda a, kv: a.replace(*kv),
                r.items(),
                meta_field_category
            )
        else:
            data = ''
    else:
        if default_meta:
            data = default_meta
        elif meta_field_tag:
            r = {'[name]': obj.name, '[total_games]': str(obj.games_published.count()), '[name2_seo]': obj.name2_seo,
                 '[name_seo_en]': obj.name_en}
            data = reduce(
                lambda a, kv: a.replace(*kv),
                r.items(),
                meta_field_tag
            )
        else:
            data = ''
    return data


@register.simple_tag
def building_template_from_db(obj, template_field):
    current_language = dict(django_settings.LANGUAGES).get(get_language())
    if template_field:
        template_field = re.sub('\[IFMOBILE\](.*?)\[ELSE\](.*?)\[ENDIF\]', r'\1' if obj.mobile else r'\2', template_field)
        template_field = re.sub('\[IFFLASH\](.*?)\[ELSE\](.*?)\[ENDIF\]', r'\1' if obj.type == 2 else r'\2', template_field)
        r = {
            '[name]': obj.name,
            '[name_en]': obj.name_en,
            '[total_plays]': str(obj.play_counter),
            '[total_likes]': str(obj.fakelike),
            '[rating]': str(obj.rating),
            '[date]': str(obj.created.strftime('%m.%d.%Y')),
            '[language]': str(current_language),
        }
        data = reduce(
            lambda a, kv: a.replace(*kv),
            r.items(),
            template_field
        )
        return safe(linebreaks(data))
    else:
        return ''


@register.filter
def get_item(dictionary, key):
    return dictionary.get(key)


@register.inclusion_tag('catalog/inclusions/_block_title.html', takes_context=True)
def new_games(context, count=16):
    return {
        'title': _('New games'),
        'objects': Game.published.all().order_by('-created')[:count],
        'request': context['request'],
        'scheme': SCHEME
    }
