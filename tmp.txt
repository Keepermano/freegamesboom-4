ALTER TABLE "catalog_tag" ALTER COLUMN "name_zh_hans" TYPE varchar(100) USING "name_zh_hans"::varchar(100);
ALTER TABLE "catalog_tag" ADD CONSTRAINT "catalog_tag_name_zh_hans_294066c6_uniq" UNIQUE ("name_zh_hans");
CREATE INDEX "catalog_tag_name_zh_hans_294066c6_like" ON "catalog_tag" ("name_zh_hans" varchar_pattern_ops);


ssh lloyd@159.69.250.112

pg_dump -Fc freegamesboom_prod -U freegamesboom_prod -h localhost > freegamesboom_prod.dump
rsync -rvz --progress lloyd@159.69.250.112:/home/lloyd/freegamesboom_prod.dump .
dropdb freegamesboom_prod
pg_restore -C -d postgres freegamesboom_prod.dump

