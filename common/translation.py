from modeltranslation.translator import translator, TranslationOptions
from .models import Settings, MenuHead, MenuFooter, MenuButton


class SettingsTranslationOptions(TranslationOptions):
    fields = (
        # 'meta_desc',
        # 'meta_keywords',
        # 'bottom_text',
        # 'tm_title_game',
        # 'tm_desc_game',
        # 'tm_title_category',
        # 'tm_desc_category',
        # 'tm_title_tag',
        # 'tm_desc_tag',
        # 'tl_desc'
    )


class MenuTranslationOptions(TranslationOptions):
    fields = ('title', )


translator.register(Settings, SettingsTranslationOptions)

translator.register(MenuHead, MenuTranslationOptions)
translator.register(MenuFooter, MenuTranslationOptions)
translator.register(MenuButton, MenuTranslationOptions)
