# -*- coding: utf-8 -*-
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.utils.encoding import iri_to_uri
from django.urls import get_script_prefix

from ckeditor.fields import RichTextField
from common.fields import CharTextField


class FlatPage(models.Model):
    title = CharTextField(_('Title'))
    url = models.CharField('URL', max_length=100, db_index=True, unique=True, help_text="/about/")
    text = RichTextField(_('Text'), blank=True)
    active = models.BooleanField(_('Active'), default=True)
    modified = models.DateTimeField(_('Modified'), auto_now=True)

    class Meta:
        verbose_name = _('Static pages')
        verbose_name_plural = _('Static page')

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return iri_to_uri(get_script_prefix().rstrip('/') + self.url)
